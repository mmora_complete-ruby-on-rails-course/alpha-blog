class UsersController < ApplicationController
    before_action :set_user, only: %i[ show edit update destroy ]
    before_action :require_user, only: [:edit, :update]
    before_action :require_same_user, only:[:edit, :update, :destroy]

    def show
        @articles = @user.articles.paginate(page: params[:page], per_page:10)
    end

    def index
      @users = User.paginate(page: params[:page], per_page:10)
    end

    def new
        @user = User.new
    end

    def edit
        @user = User.find(params[:id])
    end

    def update
        respond_to do |format|
            if @user.update(user_params)
              format.html { redirect_to user_url(@user), notice: "Your account information was successfully updated." }
              format.json { render :show, status: :ok, location: @user }
            else
              format.html { render :edit, status: :unprocessable_entity }
              format.json { render json: @user.errors, status: :unprocessable_entity }
            end
          end
    end
    def create
        @user = User.new(user_params)
        respond_to do |format|
            if @user.save
              session[:user_id] = @user.id
              format.html { redirect_to user_url(@user), notice: "Welcome to the Alpha Blog #{@user.username}, you had successfully sign up" }
              format.json { render :show, status: :created, location: @user }
            else
              format.html { render :new, status: :unprocessable_entity }
              format.json { render json: @user.errors, status: :unprocessable_entity }
            end
          end
    end

    def destroy
      @user.destroy
      session[:user_id] = nil if @user == current_user
      flash[:notice] = "Account and all associated articles successfully deleted"
      redirect_to articles_path
    end

    private
    def user_params
        params.require(:user).permit(:username, :email, :password)
    end

    def set_user
        @user = User.find(params[:id])
      end
    def require_same_user
      if current_user != @user && !current_user.admin?
        flash[:alert] = "You can edit or delete your own account"
      end
    end
end
    